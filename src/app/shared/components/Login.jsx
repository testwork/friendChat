import React from 'react';
import getFormData from 'get-form-data';
import { Link } from 'react-router';
require('es6-promise').polyfill();
require('isomorphic-fetch');

class Login extends React.Component {
	
	stateChange (env) {
		this.props.dataLogin.call(null);
	}

	render () {
		return (
			<div className="formulario">
				<form id="formulario" >
					<input className="username" type="text" name="name" placeholder="Nombre de Usuario" />
					<input className="password" type="password" name="password" placeholder="Contraseña" />
				</form>
				<Link to="/chat" className="boton-ingreso" onClick={ this.stateChange.bind(this) }>Ingresar</Link>
				<h2>Bienvenido a friendChat! donde no importa la distancia para estar con tus amigos!</h2>
			</div>
		);
	}
}

export default Login;