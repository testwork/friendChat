import React from 'react';
import $ from 'jquery';
import ListMessage from './ListMessage';
import TextSend from './TextSend';

class Conversation extends React.Component {
	volver (event) {
		this.props.mostrarUno.call(null)
	}

	render(){
		return (
			<div className={`Conversation ${ this.props.displayDos }`}>
				<button onClick={ this.volver.bind(this) }>{'<'}</button>
				<h3>{ this.props.messageContact }</h3>
				<ul>
					{
						this.props.messages.map((elemento) => {
							return <ListMessage 
										mensaje={ elemento }
										messageContact={ this.props.messageContact } />
						})
					}
				</ul>
				<TextSend send={ this.props.onSend } />
			</div>
		)
	}
}

export default Conversation;