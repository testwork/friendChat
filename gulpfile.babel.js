// -- Para NO tener problemas con los demas Archivos ------------
'use strict';

import gulp from 'gulp';
import babelify from 'babelify';
import browserify from 'browserify';
import stylus from 'gulp-stylus';
import nib from 'nib';
import minifyCSS from 'gulp-minify-css';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';

// -- Tasks --------------------------------------------

gulp.task('default', ['build', 'watch']);

gulp.task('build', ['build:js', 'build:css']);

// -- Builders
gulp.task('build:js',() => {
	return browserify({
		entries: './src/app/client/index.js',
		debug: true,
		extensions: ['.js','.jsx'],
		transform: babelify
	})
	.bundle()
	.pipe(source('app.js'))
	.pipe(buffer())
	.pipe(gulp.dest('./build/public'));
});

gulp.task('build:css', () => {
	return gulp
	.src('./src/styles/app.styl')
	.pipe(stylus({
		use: nib(),
		'include css': true,
	}))
	.pipe(minifyCSS())
	.pipe(gulp.dest('./build/public'));
});

// -- watch files

gulp.task('watch', () => {
	gulp.watch([
			'./src/app/server/index.js',
			'./src/app/client/index.js',
			'./src/app/shared/components/**/*.jsx',
			'./src/app/shared/routes.js'
		], ['build:js']);
	gulp.watch([
			'./src/styles/**/*.styl'
		], ['build:css']);
});