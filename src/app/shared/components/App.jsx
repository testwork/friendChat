import React from 'react';
import { RouteHandler, Link } from 'react-router';
import Layout from './Layout.jsx';
import getFormData from 'get-form-data';
import io from 'socket.io-client';
import Chat from './Chat';
import $ from 'jquery';
require('es6-promise').polyfill();
require('isomorphic-fetch');

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = { 
			user: false, errorMsg: '',
			errorContact: '',
			messageContact: false,
			messages: [],
			displayUno: 'display',
			displayDos: 'display_none'
		};
		this.dataLogin = this.dataLogin.bind(this);
		this.obtenerDataLogin = this.obtenerDataLogin.bind(this);
		this.agregarAmigo = this.agregarAmigo.bind(this);
		this.obtenerAgregarAmigo = this.obtenerAgregarAmigo.bind(this);
		this.changeContact = this.changeContact.bind(this);
		this.obtenerOldMessages = this.obtenerOldMessages.bind(this);
		this.obtenerAllMessages = this.obtenerAllMessages.bind(this);
		this.newMessage = this.newMessage.bind(this);
		this.onSend = this.onSend.bind(this);
		this.mostrarUno = this.mostrarUno.bind(this);
		this.mostrarDos = this.mostrarDos.bind(this);
	}
// ---------- Cambiar los display de Conversation y Friends ---

	mostrarDos() {
		this.setState({ displayUno: 'display_none', displayDos: 'display' });
	}

	mostrarUno() {
		this.setState({ displayUno: 'display', displayDos: 'display_none' });
	}

// ---------- SocketIO -----------------------------------------
	componentWillMount() {
		this.socket = io('ws://friendchat-dags.rhcloud.com:8000');
		this.socket.on('chat message', (msg) => {
			if ( msg.emisor === this.state.messageContact && msg.receptor === this.state.user.userName ) {
				this.newMessage(msg);
			}
		});
	}

// ------------ Funciones para LogIn ---------------------------------
	dataLogin() {
		let form = document.querySelector('#formulario');
		let data = getFormData(form);

		function status (response) {
			if ( response.status >= 200 && response.status < 300 ) {
				return response;
			}
			throw new Error( response.statusText )
		}

		function json(response) {
			return response.json()
		}

		fetch('/validation', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(data)
		})
			.then(status)
			.then(json)
			.then(this.obtenerDataLogin)
			.catch((error) => {
				console.log('request failed', error);
			});
	}

	obtenerDataLogin (json) {
		this.setState({ user: json.user, errorMsg: json.errorMsg });
	}

// ------------- Funciones para LogOut -------------------------

	logOut (env) {
		this.setState({ 
			user: false,
			errorMsg: '',
			errorContact: '',
			messageContact: false,
			messages: [],
			displayUno: 'display',
			displayDos: 'display_none'
		})
	}

// -- Enviar el mensaje del textArea y guardar en la db ------------------
	onSend (content) {
		let data = {
			content: content,
			emisor: this.state.user.userName,
			receptor: this.state.messageContact
		};

		function status (response) {
			if ( response.status >= 200 && response.status < 300 ) {
				return response;
			}
			throw new Error( response.statusText )
		}

		function json(response) {
			return response.json()
		}

		fetch('/mensajes', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(data)
		})
			.then(status)
			.then(json)
			.catch((error) => {
				console.log('request failed', error);
			});

		this.newMessage(data);
		this.socket.emit('chat message', data);
		$('#new-msg').val('');
	}

// -- Metodo para cambiar el state de messageContact -----------
	changeContact (value) {
		let contact = value;
		this.setState({ messageContact: contact  });
	}

// -- Metodo para agregar nuevos mensajes a la conversacion --------

	newMessage(message) {
		this.state.messages.push(message);
		let messages = this.state.messages;
		this.setState({ messages: messages });
	}

// -- Metodo para obtener todos los mensajes actualizados de la DB ----

	obtenerAllMessages ( json ) {
		if ( json ) {
			let messages = json.messages.filter((elemento) => {
				if ( elemento.emisor === this.state.user.userName && elemento.receptor === json.contact ) {
					return true
				}else if ( elemento.receptor === this.state.user.userName && elemento.emisor === json.contact ) {
					return true
				}
			});
			this.setState({ messages: messages })
		}
	}

// -- Metodo para mostrar los mensajes viejos de la conversacion ----

	obtenerOldMessages ( contacto ) {
		function status (response) {
			if ( response.status >= 200 && response.status < 300 ) {
				return response;
			}
			throw new Error( response.statusText );
		}

		function json(response) {
			return response.json();
		}

		fetch(`/chat/${ contacto }`, { method: 'POST' })
			.then(status)
			.then(json)
			.then(this.obtenerAllMessages)
			.catch(function(ex){
				console.log('parsing failed', ex);
			})
	}

// ------------- Agregar Amigo ---------------------------------

	agregarAmigo () {
		let form = document.querySelector('#agregar');
		let data = getFormData(form);

		function status (response) {
			if ( response.status >= 200 && response.status < 300 ) {
				return response;
			}
			throw new Error( response.statusText );
		}

		function json(response) {
			return response.json();
		}

		fetch('/amigo', {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(data)
		})
			.then(status)
			.then(json)
			.then(this.obtenerAgregarAmigo)
			.catch((error) => {
				console.log('request failed', error);
			});
			$('#new-amg').val('');
	}

	obtenerAgregarAmigo (json) {
		this.setState({ user: json.user, errorContact: json.errorContact })
	}


	render () {
		return (
			<Layout {...this.props }>
				<header>
					<div className="titulo-principal">
						<h1>{ this.props.title }</h1>
						<img className="imagen-titulo" src="/images/chat.png" />
					</div>
				</header>

				{
					(() =>{
						if (this.state.user) {
							return (
								<div className="navegacion-secundaria">
									<h1 className="nombre">{this.state.user.userName}</h1>
									<Link to="/" className="boton-ingreso" onClick={ this.logOut.bind(this) }>Logout</Link>
								</div>
							)
						}else{
							return(
								<nav className="navegacion-principal">
									<li><Link to="/register">Registrarse</Link></li>
									<li><Link to="/chat">Ya tienes cuenta?</Link></li>
								</nav>
							)
						}
					})()
				}

				<main role="application">
					<RouteHandler {...this.props } 
						dataLogin={ this.dataLogin }
						agregarAmigo={ this.agregarAmigo }
						user={ this.state.user }
						messageContact={ this.state.messageContact }
						changeContact={ this.changeContact }
						errorMsg={ this.state.errorMsg }
						errorContact={ this.state.errorContact }
						obtenerOldMessages={ this.obtenerOldMessages }
						messages={ this.state.messages }
						onSend={ this.onSend }
						displayUno={ this.state.displayUno }
						mostrarUno={ this.mostrarUno }
						displayDos={ this.state.displayDos }
						mostrarDos={ this.mostrarDos } />

				</main>
			</Layout>
		);
	}
}

export default App;