import React from 'react';

class Layout extends React.Component {
	render () {
		return (
				<html lang='es'>
					<head>
						<meta charset='utf-8' />
						<title>{ this.props.title }</title>
						<meta name="Chat" content="" />
						<meta name="viewport" content="width=device-width, initial-scale=1" />
						<link rel="stylesheet" href="/normalize.css" type="text/css" />
						<link rel="stylesheet" href="/app.css" type="text/css" />
						<link rel="shorcut icon" href="/favicon.ico" type="image/x-icon" />
					</head>
					<body>
						{ this.props.children }
						<script src='/app.js'></script>
					</body>
				</html>
			);
	}
}

export default Layout;