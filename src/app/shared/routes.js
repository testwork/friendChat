import React from 'react';
import { Route, DefaultRoute } from 'react-router';

// -- View Components -----------------------
import App from './components/App.jsx';
import Register from './components/Register.jsx';
import Chat from './components/Chat.jsx';


const routes = (
	<Route path='/' handler={ App }>
		<DefaultRoute handler={ Chat } />
		<Route path='/chat' handler={ Chat } />
		<Route path='/chat/:contact' handler={ Chat } />
		<Route path='/register' handler={ Register } />
	</Route>
);

export default routes;