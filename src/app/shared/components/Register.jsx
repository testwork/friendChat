import React from 'react';

class Register extends React.Component {
	render () {
		return(
			<div className="formulario">
				<form method="POST" action="/register">
					<input className="username" type="text" name="name" placeholder="Nombre de Usuario" required />
					<input className="password" type="password" name="password" placeholder="Contraseña" required />
					<input className="boton-ingreso" type="submit" value="Registrar" />
				</form>
			</div>
		)
	}
}

export default Register;