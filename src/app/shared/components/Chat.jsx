import React from 'react';
import getFormData from 'get-form-data';
import Login from './Login.jsx';
import Friends from './Friends';
import Conversation from './Conversation';
import $ from 'jquery';
import io from 'socket.io-client';
require('es6-promise').polyfill();
require('isomorphic-fetch');

class Chat extends React.Component {
	constructor(props){
		super(props);
		this.state = { userContact: [] };
	}

	componentWillReceiveProps(){
// -- Sacar la informacion individual de los contactos ----------
		if ( this.props.user ) {
			let userContact = [];
			this.props.users.map((elemento) => {
				this.props.user.contacts.map((elemento2) => {
					if ( elemento.userName === elemento2 ) {
						userContact.push(elemento);
					}
				})
			})
			this.setState({ userContact: userContact });
		}
	}

	render () {
		if (!this.props.user) {
			return (
				<div>
					<Login dataLogin={ this.props.dataLogin } />
					<p id="error_login">{ this.props.errorMsg }</p>
				</div>
			)
		} else {
			return (
				<div className="Chat">
					<Friends 
						user={ this.props.user }
						errorContact={ this.props.errorContact }
						agregarAmigo={ this.props.agregarAmigo }
						changeContact={ this.props.changeContact }
						obtenerOldMessages={ this.props.obtenerOldMessages }
						displayUno={ this.props.displayUno }
						mostrarDos={ this.props.mostrarDos } />
					{
						(() => {
							if ( this.props.messageContact ) {
								return (
									<Conversation 
										user={ this.props.user } 
										users={ this.props.users } 
										messages={ this.props.messages }
										onSend={ this.props.onSend }
										messageContact={ this.props.messageContact }
										displayDos={ this.props.displayDos }
										mostrarUno={ this.props.mostrarUno } />
								)
							}
						})()
					}
				</div>
			)
		}
	}
}

export default Chat;