import React from 'react';
import FriendsRow from './FriendsRow';
import getFormData from 'get-form-data';
require('es6-promise').polyfill();
require('isomorphic-fetch');

class Friends extends React.Component{
	
	agregar(env) {
		this.props.agregarAmigo.call(null);
	}

	render () {
		if ( this.props.user ) {
			return(
				<div className={`Friends ${ this.props.displayUno }`}>
					<h2>Lista de Amigos</h2>
					<form id="agregar">
						<input type="text" name="agregar" id="new-amg" placeholder="Nombre de Amigo" />
						<input type="hidden" name="user" value={ this.props.user.userName } />
					</form>
					<button onClick={ this.agregar.bind(this) }>Agregar Amigo</button>
					{
						(() =>{
							if ( this.props.errorContact.length > 0 ) {
								return <p>{ this.props.errorContact }</p>
							}
						})()
					}
					<ul>
						{
							this.props.user.contacts.map((elemento) => {
								return <FriendsRow 
											name={ elemento }
											changeContact={ this.props.changeContact }
											obtenerOldMessages={ this.props.obtenerOldMessages }
											mostrarDos={ this.props.mostrarDos } />
							})
						}
					</ul>
				</div>
			)
		}else{
			return <h1>No funca  </h1>
		}
		
	}
}

export default Friends;