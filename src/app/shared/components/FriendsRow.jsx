import React from 'react';
import { Link } from 'react-router';

class FriendsRow extends React.Component {

	contacto (event) {
		this.props.changeContact.call(null, this.props.name)
		this.props.obtenerOldMessages.call(null, this.props.name)
		this.props.mostrarDos.call(null)
	}
	
	render (){
		return(
			<Link to={`/chat/${this.props.name}`} className="lista" onClick={ this.contacto.bind(this) }>
				<li className="FriendsRow">
					 { this.props.name }
				</li>
			 </Link>
		)
	}
}

export default FriendsRow;