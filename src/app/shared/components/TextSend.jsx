import React from 'react';
import $ from 'jquery';

class TextSend extends React.Component {
	onClick(ev) {
		this.props.send.call(null, $('#new-msg').val())
	}
	render() {
		return <div className="TextSend">
				<textarea id="new-msg" cols="30" rows="2" placeholder="Introduzca su mensaje aqui" />
				<button onClick={this.onClick.bind(this)}>></button>
			</div>
	}
}

export default TextSend;