import Client from 'react-engine/lib/client';
import routes from '../shared/routes.js';

// -- Boot Options --------------------------------------------------

const options = { routes };

// -- Para que las vistas ya esten cargadas en el server --------------
document.addEventListener('DOMContentLoaded', function onLoad() {
	Client.boot(options);
});