const config = {
	app: {
		name: 'friendChat',
		port: process.env.OPENSHIFT_NODEJS_PORT || '3000',
		ip_addr: process.env.OPENSHIFT_NODEJS_IP   || '127.0.0.1'
	},

	api: 'http://localhost:3001'
};

export default config;