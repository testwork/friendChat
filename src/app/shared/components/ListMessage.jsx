import React from 'react';

class ListMessage extends React.Component {
	render() {
		let estilo;
		if ( this.props.mensaje.emisor === this.props.messageContact ) {
			estilo = 'emisor';
		}else{
			estilo = 'receptor';
		}
		return(
			<li className={`${estilo}`}>
				{
					(()=>{
						if ( this.props.mensaje.emisor === this.props.messageContact ) {
							return (
								<p> {`${ this.props.mensaje.content }`} </p>
							)
						}else{
							return(
								<p> {` ${ this.props.mensaje.content }`} </p>
							)
						}
					})()
				}
			</li>
		)
	}
}

export default ListMessage;