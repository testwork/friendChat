import path from 'path';
import express from 'express';
import engine from 'react-engine';
import favicon from 'serve-favicon';
import config from '../config';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import realTime from 'socket.io';
import http from 'http';

const app = express();
const port = config.app.port;
const ip = config.app.ip_addr;

// -- Setup React Views Engine ---------------------

app.engine('.jsx', engine.server.create({
	reactRoutes: path.join(__dirname, '..', 'shared', 'routes.js')
}));
app.set('views', path.join(__dirname,'..', 'shared', 'components'));
app.set('view engine', 'jsx');
app.set('view', engine.expressView);

// -- Routes & Middlewares -------------------------

let publicPath = path.join(__dirname, '..', '..', '..', 'build', 'public');
app.use(express.static(publicPath));

let faviconPath = path.join(__dirname, '..', '..', '..', 'build', 'public', 'favicon.ico');
app.use(favicon(faviconPath));

// -- Usar BodyParser para decodificar los POST ----------------------

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// -- Conectarse a la DB con MongoDB

// default to a 'localhost' configuration:
var connection_string = '127.0.0.1:27017/friendChat';
// if OPENSHIFT env variables are present, use the available connection info:
if(process.env.OPENSHIFT_MONGODB_DB_PASSWORD){
  connection_string = process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
  process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
  process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
  process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
  process.env.OPENSHIFT_APP_NAME;
}

mongoose.connect(`mongodb://${connection_string}`);

// -- Creacion de Schemas --------------------------------------------

var userSchema = new mongoose.Schema(
	{
		userName: String,
		password: String,
		contacts: Array
	}
);

var messageSchema = new mongoose.Schema(
	{
		emisor: String,
		receptor: String,
		content: String
	}
);

// -- Creacion de modelos --------------------------------------------

var users = mongoose.model('users', userSchema);
var messages = mongoose.model('messages', messageSchema);

// -- Declaracion de Todas las Rutas que se la envie a React ------------------

app.get('*', (req, res) => {
	users.find((err, userData) => {
		if (err) { console.log(err); }
		let usuarios = userData;
		messages.find((err, messageData) => {
			if (err) { console.log(err); }
			let mensaje = messageData;
			res.render(req.url, {
				title: config.app.name,
				users: usuarios,
				messages: mensaje
			});
		});
	});
});

// -- Rutas para recibir  peticiones POST ------------

// -- Crear Usuarios ---------------------------------

app.post('/register', (req, res) => {
	let data = {
		userName: req.body.name,
		password: req.body.password
	};

	let user = new users(data);
	user.save((err) => {
		//console.log(user)
		res.redirect('/chat');
	});
});

// -- Login de usuarios ------------------------------

app.post('/validation', (req, res) => {
	let data = {
		userName: req.body.name,
		password: req.body.password
	};
	users.findOne({'userName': data.userName }, (err, userData) => {
		if( userData === null ) {
			res.send({'error': true, 'user': false, 'errorMsg': 'Usuario o contraseña incorrecta' });
		} else {
			if ( userData.password === req.body.password ) {
				res.send({'error': false, 'user': userData });
			} else {
				res.send({'error': true, 'user': false, 'errorMsg': 'Usuario o contraseña incorrecta' });
			}
		}
	});
});

// -- Agregar Amigo ----------------------------------

app.post('/amigo', (req, res) => {
	let data = {
		amigo: req.body.agregar,
		user: req.body.user
	};
	//console.log(data.amigo);
	users.findOne({ 'userName': data.user }, (err, userData) => {
		let usuario = userData;
		users.findOne({ 'userName': data.amigo }, (err, amigoData) => {
			if ( amigoData ===  null ) {
				res.send({ errorContact: 'No existe', user: usuario })
			}else{
				let contactos = usuario.contacts.filter((elemento) => {
					if ( amigoData.userName === elemento ) {
						return true
					}
				})
				if ( contactos.length > 0 ) {
					res.send({ errorContact: 'Esta en tus lista', user: usuario });
				}else{
					usuario.contacts.push( amigoData.userName )
					users.update({ 'userName': data.user }, usuario, () => {
						//console.log(usuario);
						res.send({ errorContact: '', user: usuario });
					});
				}
			}
		});
	});
});

// -- Cargar los Mensajes de la conversacion de base de datos ---------

app.post('/chat/:contact', (req, res) => {
	messages.find((err, messagesData) => {
		if (err) {console.log(err);}
		let mensajes = messagesData;
		res.send({ messages: mensajes, contact: req.params.contact })
	})
});

// -- Agregar mensaje a la base de datos -----------------

app.post('/mensajes', (req, res) => {
	let data = {
		content: req.body.content,
		emisor: req.body.emisor,
		receptor: req.body.receptor
	};
	let message = new messages(data);
	message.save((err) => {
		if (err) {console.log(err);}
		//console.log(message);
		res.send({ respuesta: 'empty' })
	})
});

// -- Start the application server -------------------

let server = http.createServer(app).listen(port, ip, () => {
	console.log(`El servidor esta escuchando en puerto ${port}`);
});

const io = realTime.listen(server);

// -- Socket.io a la escucha de nuevas conexiones -------

io.on('connection', (socket) => {
	//console.log('New User connected');

	/**
		Cada nuevo socket debera estar a la escucha del evento 'chat message',
		el cual se activa cada vez que un usuario envia un mensaje

		@params msg : Los datos enviados desde el cliente a traves del socket.
	*/

	socket.on('chat message', (msg) => {
		io.emit('chat message', msg);
	});

	// -- Mostramos en consola cada vez que un usuario se desconecte ---------

	/*socket.on('disconnect', () => {
		console.log('User disconnected');
	});*/
});
